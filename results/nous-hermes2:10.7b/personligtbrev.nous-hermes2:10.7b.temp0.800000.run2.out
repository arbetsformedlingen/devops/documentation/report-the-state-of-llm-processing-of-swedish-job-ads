Ansökan - Ansökning till Lageransvarig Arlöv

Dear Testia,

Jag ville ansluta min intresse för att söka om positionen som Lageransvarig på er arbetsplats i Arlöv. Efter att ha granskat din jobbannonst och webbplats, tycker jag att mitt bakgrund och erfarenhet passar bra för denna roll.

Med min magisterexamen i Datavetenskap från Uppsala universitet, har jag utvecklat en stark teknikkunskap som jag tror skulle komma till användning när det gäller ansvariga för kontering och granskning av fakturor samt att säkerställa löpande att inskrivning av reservdelar sker och att lagersaldon är korrekta.

Min erfarenhet som nätverksutvecklare hos Ericsson gav mig också en stark förmåga att arbeta i team och leverera högkvalitativa lösningar, vilket skulle vara av stor hjälp för er då ni söker en ansvarig som kan bistå med 5S-lagring och samarbete med SQA-ansvariga för att ge förutsättningar/material att driva SQA.

Jag är mycket intresserad av att ansluta mig till Testia på grund av er affärsidé att "Förenkla kundens vardagsresor" och strävan mot er vision att "Alla vill resa med oss". Jag tror att jag skulle kunna bidra positivt till er organisation och arbetsmiljö, samtidigt som jag kan läras av er erfarenheter och kompetens inom kollektivtrafik.

Jag är öppen för intervju och vill tacka er för att ta reda på min ansökan. Jag hoppas att vi snart får chansen att träffas och diskutera hur jag kan bidra till Testias framgång som Lageransvarig i Arlöv.

Med vänliga hälsningar,
Test Testsson

(English translation:)

Application - Application for the position of Warehouse Manager in Arlöv

Dear Testia,

I am writing to apply for the position of Warehouse Manager at your facility in Arlöv. After reviewing your job posting and company website, I believe my background and experience are well-suited for this role.

With my Master's degree in Computer Science from Uppsala University, I have developed strong technical skills that I think would be beneficial when it comes to being responsible for accounting and reviewing invoices as well as ensuring that inventory recording is done on an ongoing basis and that stock levels are correct.

My experience as a network developer at Ericsson also gave me a strong ability to work in teams and deliver high-quality solutions, which would be very helpful for you as you seek a manager who can assist with 5S warehousing and collaboration with SQA managers to provide materials/resources for driving SQA.

I am very interested in joining Testia because of your business philosophy of "Simplifying the customer's daily travel" and your vision that "Everyone wants to travel with us." I believe that I can positively contribute to your organization and work environment while also learning from your experiences and expertise in public transportation.

I am open to an interview and would like to thank you for considering my application. I hope we will have the opportunity to meet soon and discuss how I can contribute to Testia's success as a Warehouse Manager in Arlöv.

Sincerely,
Test Testsson

