package main

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"io"
	"os/exec"
	"strconv"
	"strings"
	"text/template"

	"github.com/spf13/pflag"
)

type Args struct {
	ConfName     string
	Input        string
	Prompt       string
	models       []string
	temperatures []float64
	OutputDir    string // New CLI argument for output directory
	deleteModels   bool
	iters        int
}

type ModelFile struct {
    Model       string
    Temperature float64
    Prompt      string
}

func parseArgs() Args {
	// Define default values
	defaultConfName := "personligtbrev"
	defaultInput := "data/input1.txt"
	defaultPrompt := "Du är en expert på att skriva svenska ansökningsbrev givet ett CV och en jobbannons, så att det så gott som möjligt förstås att den sökande är en lämplig kandidat för jobbet givet sin bakgrund. Du skriver breven på svenska. "
	defaultOutputDir := "results" // Default output directory

	var confName string
	var input string
	var prompt string
	var modelList string
	var temperatureList string
	var outputDir string
	var deleteModels bool
	var iters int

	// Parse command line options
	pflag.StringVarP(&confName, "conf", "c", defaultConfName, "Configuration name")
	pflag.StringVarP(&input, "input", "i", defaultInput, "Input file")
	pflag.StringVarP(&prompt, "prompt", "p", defaultPrompt, "Prompt message")
	pflag.StringVarP(&modelList, "models", "m", "", "Comma-separated list of models")
	pflag.StringVarP(&temperatureList, "temperatures", "t", "", "Comma-separated list of temperatures")
	pflag.StringVarP(&outputDir, "output", "o", defaultOutputDir, "Output directory") // New CLI argument for output directory
	pflag.BoolVarP(&deleteModels, "delete-models", "k", false, "delete models")
	pflag.IntVarP(&iters, "iters", "n", 10, "Iterations for each model and temperature")
	pflag.Parse()

	models := []string{}
	if modelList != "" {
		models = strings.Split(modelList, ",")
	} else {
		models = []string{
			"llama3:70b-instruct-q5_K_M",
			"mistral:7b-instruct-q5_K_M",
			"mixtral:8x22b-instruct-v0.1-q3_K_S",
			"llava:7b-v1.5-q5_K_M",
			"mistral-openorca:7b-q5_K_M",
			"nous-hermes2:10.7b",
			"dolphin-llama3:70b-v2.9-q5_K_M",
			"tinyllama:1.1b-chat",
			"openhermes:7b-mistral-v2-q5_K_M",
			"tinydolphin:1.1b",
			"falcon2:11b-q5_K_M",
			//"./models/gpt-sw3-6.7b-v2-instruct-Q5_K_M.gguf",
		}
	}

	temperatures := []float64{}
	if temperatureList != "" {
		temperatureStrings := strings.Split(temperatureList, ",")
		for _, temp := range temperatureStrings {
			tempFloat, err := strconv.ParseFloat(temp, 64)
			if err != nil {
				panic(fmt.Errorf("error parsing temperature: %w", err))
			}
			temperatures = append(temperatures, tempFloat)
		}
	} else {
		temperatures = []float64{
			0.4, 0.6, 0.8, 1.0,
		}
	}

	return Args{
		ConfName:       confName,
		Input:          input,
		Prompt:         prompt,
		models:         models,
		temperatures:   temperatures,
		OutputDir:      outputDir, // Assign the output directory from CLI argument
		deleteModels:     deleteModels,
		iters:		    iters,
	}
}


func createModelFile(template_file string, model string, temp float64, prompt string, outputFileName string) {
    tmpl, err := template.ParseFiles(template_file)
    if err != nil {
        log.Fatal(err)
    }

	data := ModelFile{
		Model:       model,
		Temperature: temp,
		Prompt:      prompt,
	}

	file, err := os.Create(outputFileName)
	if err != nil {
		log.Fatal(err)
	}

	err = tmpl.Execute(file, data)
	if err != nil {
		log.Fatal(err)
	}

	file.Close()
}

func model_exists(name string) bool {
	cmd := exec.Command("ollama", "list")
	output, err := cmd.Output()
	if err != nil {
		log.Fatal(err)
	}

	models := strings.Split(string(output), "\n")
	for _, model := range models {
		if strings.HasPrefix(model, name) && (len(model) == len(name) || model[len(name)] == ' ' || model[len(name)] == '\t') {
			return true
		}
	}

	return false
}

func main() {
	args := parseArgs()

	os.MkdirAll(args.OutputDir, os.ModePerm)

	var max_iters = args.iters * len(args.models) * len(args.temperatures)
	var iter = 0

	for _, model := range args.models {
		var symName string = ""
		if strings.HasSuffix(model, ".gguf") {
			symName = model[strings.LastIndex(model, "/")+1:]
		} else {
			symName = strings.ReplaceAll(strings.TrimPrefix(model, "./"), "/", "_")
		}
		os.MkdirAll(args.OutputDir+"/"+symName, os.ModePerm)

		for _, temp := range args.temperatures {
			var created_model = 0
			var model_name = args.ConfName+"_"+symName+"_"+fmt.Sprintf("%f", temp)
			for n := 1; n <= args.iters; n++ {
				log.Printf("[%d/%d]\tmodel %s\ttemperature %.2f\titeration %d", iter, max_iters, model, temp, n)
				iter++
				dest_file := fmt.Sprintf("%s/%s/%s.%s.temp%f.run%d.out", args.OutputDir, symName, args.ConfName, symName, temp, n)
				if _, err := os.Stat(dest_file); os.IsNotExist(err) {
					log.Printf("Creating file %s", dest_file)
					if !model_exists(model) {
						log.Printf("Pulling model %s", model)
						exec.Command("ollama", "pull", model).Run()
					}
					if _, err := os.Stat(args.OutputDir+"/"+symName+"/SYSTEM"); errors.Is(err, os.ErrNotExist) {
						OutputToFile(exec.Command("ollama", "show", "--license", model), args.OutputDir+"/"+symName+"/LICENSE")
						OutputToFile(exec.Command("ollama", "show", "--system", model), args.OutputDir+"/"+symName+"/SYSTEM")
						OutputToFile(exec.Command("ollama", "list"), args.OutputDir+"/"+symName+"/LIST")
					}
					if !model_exists(model_name) {
						log.Printf("Creating model %s", model_name)
						model_file := fmt.Sprintf("%s/%s/%s.%s.temp%f.modelfile", args.OutputDir, symName, args.ConfName, symName, temp)
						createModelFile("data/Modelfile.template", model, temp, args.Prompt, model_file)
						cmd := exec.Command("ollama", "create", model_name, "-f", model_file)
						output, err := cmd.CombinedOutput()
						if err != nil {
							log.Printf("Command failed with error: %v. Output: %s", err, output)
							continue
						}		
						created_model = 1
					}
					
					OutputToFileWithAllIO(
						exec.Command("/usr/bin/time", "-o", fmt.Sprintf("%s/%s/%s.%s.temp%f.run%d.time", args.OutputDir, symName, args.ConfName, symName, temp, n), "-p", "ollama", "run", model_name),
						dest_file,
						fmt.Sprintf("%s/%s/%s.%s.temp%f.run%d.err", args.OutputDir, symName, args.ConfName, symName, temp, n),
						args.Input,
					)
				} else {
					log.Printf("Skipping existing file %s", dest_file)
				}
			}
			if args.deleteModels && created_model == 1 {
				exec.Command("ollama", "rm", model_name).Run()
			}	
		}

		if args.deleteModels {
			exec.Command("ollama", "rm", symName).Run()
		}
	}
}

func OutputToFile(c *exec.Cmd, filename string) {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		out, _ := c.Output()
		os.WriteFile(filename, out, os.ModePerm)
	}
}

func OutputToFileWithAllIO(c *exec.Cmd, stdoutFile string, stderrFile string, inputFile string) {
	log.Printf("exec: %s %s %s %s", c, stdoutFile, stderrFile, inputFile)
	if _, err := os.Stat(stdoutFile); os.IsNotExist(err) {
		// Read from the input file
		inputBytes, err := os.ReadFile(inputFile)
		if err != nil {
			log.Fatal(err)
		}
		c.Stdin = bytes.NewReader(inputBytes)
		stdout, _ := c.StdoutPipe()
		stderr, _ := c.StderrPipe()

		// Start the command
		if err := c.Start(); err != nil {
			log.Fatal(err)
		}

		// Read stdout and stderr in separate goroutines
		stdoutBytes := make(chan []byte)
		stderrBytes := make(chan []byte)
		go func() {
			b, _ := io.ReadAll(stdout)
			stdoutBytes <- b
		}()
		go func() {
			b, _ := io.ReadAll(stderr)
			stderrBytes <- b
		}()

		// Wait for the command to finish
		if err := c.Wait(); err != nil {
			log.Fatal(err)
		}

		// Write the output to files
		var errBytes []byte = <-stderrBytes
		os.WriteFile(stdoutFile, <-stdoutBytes, os.ModePerm)
		os.WriteFile(stderrFile, errBytes, os.ModePerm)
		log.Println(string(errBytes))
	}
}
