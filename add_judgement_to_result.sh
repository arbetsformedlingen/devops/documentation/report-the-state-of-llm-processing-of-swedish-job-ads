#!/bin/bash

while IFS= read -r line; do
    # Extract the filename from the line
    filename=$(echo "$line" | jq -r '.filename')

    # Append the .openai_judgement suffix to the filename
    judgement_file="${filename}.openai_judgement"

    # Read the content of the .openai_judgement file if it exists
    if [ -f "$judgement_file" ]; then
        judgement=$(<"$judgement_file")
    else
        judgement="N/A"
    fi

    case "$judgement" in
    "BRA")
        judgement=true
        ;;
    "DÅLIGT")
        judgement=false
        ;;
    *)  echo "Invalid judgement: $judgement for file: $filename" >&2
        ;;
    esac
    
    # Add the judgement attribute to the JSON object
    updated_line=$(echo "$line" | jq -c --arg judgement "$judgement" '. + { "acceptable": $judgement }')

    # Print the updated line
    echo "$updated_line"
done
