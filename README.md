# LLM model runner

This project is a Go application that interacts with the Ollama server to manage and run models. It creates directories for each model, pulls the model from the Ollama server, and runs the model for different temperatures. The results are saved in the output directory.

## Prerequisites
Before you begin, ensure you have met the following requirements:

- You have installed the latest version of Go.
- You have a Linux or Mac machine. Windows is not currently supported.
- You have read the LICENSE file.
- You have the ollama command available in your $PATH. This means you should be able to run ollama from your terminal.
- You have a running instance of the Ollama server.

## Building the Project
To build the project, navigate to the project directory and run the following command:
```
go build main.go
```

This will create an executable named `main` in your current directory.

## Running the Project

To get a list of accepted arguments, use `-h`:
```
./main  -h
Usage of ./main:
  -c, --conf string           Configuration name (default "personligtbrev")
  -i, --input string          Input file (default "data/input1.txt")
  -m, --models string         Comma-separated list of models
  -o, --output string         Output directory (default "results")
  -p, --prompt string         Prompt message (default "Du är en expert på att skriva svenska ansökningsbrev givet ett CV och en jobbannons, så att det så gott som möjligt förstås att den sökande är en lämplig kandidat för jobbet givet sin bakgrund. Du skriver breven på svenska. ")
  -t, --temperatures string   Comma-separated list of temperatures
```

To run the project, **you need to have a running instance of the Ollama server**. Once the server is running, you can execute the project using the following command, with any optional arguments from the list above.

You can supply the path to a GGUF file as a model as well, preferably with an absolute path.
```
./main
```

This will start the application and begin interacting with the Ollama server.

## License
This project uses the GNU General Public License v3.0. For more details, please see the LICENSE file.

