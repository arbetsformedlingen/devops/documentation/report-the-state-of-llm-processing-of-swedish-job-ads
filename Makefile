.PHONY: gen-openai

tmpf = /tmp/tmpf

result.sort.jsonl: result.jsonl
	< $< jq -cs 'sort_by( (.empty == false | not), (.enough_words == true | not), (.correct_lang == true | not), (.acceptable == "true" | not), -.similarity, .spell_fix_ratio ) | .[]' >$@


result.jsonl: base_relevance.jsonl
	< $< bash add_judgement_to_result.sh >$@


base_relevance.jsonl:
	. ../calc-relevance/.venv/bin/activate \
	&& find results -name '*.out' \
	| python ../calc-relevance/src/relevance/__main__.py --reference data/input1.txt -i >$@

gen-openai-judgement:
	. ../openai-client/secret.sh \
	&& export OPENAI_API_KEY \
	&& for F in $$(find results -type f -name '*.out'); do if [ ! -f "$$F".openai_judgement ]; then /usr/bin/echo -e "Här är ansökningbrevet som ska bedömas:\n" > $(tmpf); cat < $$F >> $(tmpf); ../openai-client/openai-client --prompt "Du är en expert på att bedöma kvaliteten på ansökningsbrev genererade av datorprogram. Titta på ansökningsbrevet och svara med BRA eller DÅLIGT - ingen övrig text ska svaret innehålla. Ett bra ansökningsbrev ska vara skrivet på svenska, ha ett vårdat språk och rent allmänt vara ett bra ansökningsbrev. " --input $(tmpf) | tee $$F.openai_judgement; fi; done

## including the candidate systems input didn't work any better
#	&& for F in $$(find results -type f -name '*.out' | head -n 20); do if [ ! -f "$$F".openai_judgement2 ]; then /usr/bin/echo -e "Här är ansökningbrevet som ska bedömas:\n" > $(tmpf); cat < $$F >> $(tmpf); /usr/bin/echo -e "\n\nHär följer instruktionen till systemet som skrev ansökningsbrevet - använd den för att avgöra om ansökningsbrevet är relevant:\n\nsvenska ansökningsbrev skapas givet ett CV och en jobbannons, så att det så gott som möjligt förstås att den sökande är en lämplig kandidat för jobbet givet sin bakgrund. breven ska vara på svenska. " >> $(tmpf); cat $(PWD)/data/input1.txt >> $(tmpf) ; ../openai-client/openai-client --prompt "Du är en expert på att bedöma kvaliteten på ansökningsbrev genererade av datorprogram. Titta på ansökningsbrevet och svara med BRA eller DÅLIGT - ingen övrig text ska svaret innehålla. Ett bra ansökningsbrev ska vara skrivet på svenska, ha ett vårdat språk och rent allmänt vara ett bra ansökningsbrev. " --input $(tmpf) | tee $$F.openai_judgement2; fi; done




gen-openai:
	mkdir -p results/openai-gpt4o
	. ../openai-client/secret.sh \
	&& export OPENAI_API_KEY \
	&& for N in $$(seq 1 10); do /usr/bin/time -p \
	-o results/openai-gpt4o/openai-gpt4o.run"$$N".time \
	../openai-client/openai-client --prompt "Du är en expert på att skriva svenska ansökningsbrev givet ett CV och en jobbannons, så att det så gott som möjligt förstås att den sökande är en lämplig kandidat för jobbet givet sin bakgrund. Du skriver breven på svenska. " --input $(PWD)/data/input1.txt \
	> results/openai-gpt4o/openai-gpt4o.run"$$N".out \
	2> results/openai-gpt4o/openai-gpt4o.run"$$N".err \
	; done
